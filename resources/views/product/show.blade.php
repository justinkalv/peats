@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-offset-3 thumbnail">
			<h3><strong>{{ $products->title }}</strong></h3>

			<img src="{{ $products->photo }}" class="img-responsive" width="500" >
			<p><strong>{{$products->description}}</strong></p>
			<ul>
				<li><strong>Price: {{$products->price}} eur</strong></li>
				<li><strong>Quantity: {{$products->quantity}} eur</strong></li>
			</ul>
		</div>
	</div>
</div>
@endsection