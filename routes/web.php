<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AboutController@index');


Auth::routes();

Route::get('/home', 'ProductController@index');

Route::resource('products', 'ProductController');

Route::resource('orders', 'OrderController');

Route::resource('contacts', 'ContactController');

Route::resource('about', 'AboutController');	